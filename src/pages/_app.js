import { Provider } from 'react-redux';
import store from 'app/store';

import 'styles/index.scss';

const App = ({ Component, pageProps }) => {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
};

export default App;
