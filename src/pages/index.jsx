import Head from 'next/head';
import { Counter } from 'features/counter/Counter';

const Home = () => {
  return (
    <div>
      <Head>
        <title>Create Next App</title>
      </Head>
      <div className="d-flex justify-content-center">Welcome!</div>
      <Counter />
    </div>
  );
};

export default Home;
